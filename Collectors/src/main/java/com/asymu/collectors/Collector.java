package com.asymu.collectors;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Collector {

    public static void main(String[] args) {
//        examples();
        fileReader();
    }

    static void examples () {
        String joined = List.of("one", "two", "three", "four", "five")
                .stream()
                .collect(Collectors.joining());
        System.out.println(joined);

        joined = List.of("one", "two", "three", "four", "five")
                .stream()
                .collect(Collectors.joining(",", "Prefix", "Suffix"));
        System.out.println(joined);

        Set<String> mapped = List.of("one", "two", "three", "four", "five", "three")
                .stream()
                .collect(Collectors.mapping((s) -> { return s + "-suffix"; }, Collectors.toSet()));
        System.out.println(mapped);

        List<String> flatmapped = List.of(
                Set.of("one", "two", "three"),
                Set.of("four", "five"),
                Set.of("six"),
                Set.of("six")
        )
                .stream()
                .collect(Collectors.flatMapping(
                        (set) -> { return set.stream(); },
                        Collectors.toList())
                );
        System.out.println(flatmapped);

        Set<String> collected = List.of("Syed", "Mudassir", "Mudassir Nazar")
                .stream()
                .collect(Collectors.filtering(
                        (name) -> { return name.length() < 9; },
                        Collectors.toSet())
                );
        System.out.println(collected);

        Set<String> immutableSet = List.of("Syed", "Mudassir", "Mudassir Nazar")
                .stream()
                .collect(Collectors.collectingAndThen(
                        Collectors.toSet(),
                        (set) -> { return Collections.unmodifiableSet(set); })
                );
        System.out.println(immutableSet);

        long count = List.of("Syed", "Mudassir", "Mudassir Nazar")
                .stream()
                .collect(Collectors.counting());
        System.out.println(count);

        Optional<Integer> smallest = Stream.of(1, 2, 3)
                .collect(Collectors.minBy((a, b) -> { return a - b; }));
        System.out.println(smallest);

        Optional<Integer> biggest = Stream.of(1, 2, 3)
                .collect(Collectors.maxBy((a, b) -> { return a - b; }));
        System.out.println(biggest);

        int sumInt = Stream.of(1d, 2d, 3d)
                .collect(Collectors.summingInt((d) -> { return d.intValue(); }));
        System.out.println(sumInt);

        long sumLong = Stream.of(1d, 2d, 3d)
                .collect(Collectors.summingLong((d) -> { return d.longValue(); }));
        System.out.println(sumLong);

        double sumDouble = Stream.of(1, 2, 3)
                .collect(Collectors.summingDouble((i) -> { return i.doubleValue(); }));
        System.out.println(sumDouble);

        double averagingInt = Stream.of(1d, 2d, 3d)
                .collect(Collectors.averagingInt((d) -> { return d.intValue(); }));
        System.out.println(averagingInt);

        double averagingLong = Stream.of(1d, 2d, 3d)
                .collect(Collectors.averagingLong((d) -> { return d.longValue(); }));
        System.out.println(averagingLong);

        double averagingDouble = Stream.of(1, 2, 3)
                .collect(Collectors.averagingDouble((i) -> { return i.doubleValue(); }));
        System.out.println(averagingDouble);

        Map<String, Set<Car>> grouped = Stream.of(
                new Car("Toyota", 92),
                new Car("Kia", 104),
                new Car("Hyundai", 89),
                new Car("Toyota", 116),
                new Car("Mercedes", 209))
                .collect(Collectors.groupingBy(Car::getBrand, Collectors.toSet()));
        System.out.println(grouped);

        Map<String, Optional<Car>> collectedGroup = Stream.of(
                new Car("Volvo", 195),
                new Car("Honda", 96),
                new Car("Volvo", 165),
                new Car("Volvo", 165),
                new Car("Honda", 104),
                new Car("Honda", 201),
                new Car("Volvo", 215))
                .collect(Collectors.groupingBy(Car::getBrand, Collectors.reducing((carA, carB) -> {
                    if (carA.enginePower > carB.enginePower) {
                        return carA;
                    }
                    return carB;
                })));
        System.out.println(collectedGroup);

        Map<Boolean, List<Car>> partitioned = Stream.of(
                new Car("Toyota", 92),
                new Car("Kia", 104),
                new Car("Hyundai", 89),
                new Car("Toyota", 116),
                new Car("Mercedes", 209))
                .collect(Collectors.partitioningBy((car) -> { return car.enginePower > 100; }));
        System.out.println(partitioned);

        Map<Boolean, Set<Car>> partitionedSet = Stream.of(
                new Car("Toyota", 92),
                new Car("Kia", 104),
                new Car("Hyundai", 89),
                new Car("Toyota", 116),
                new Car("Mercedes", 209))
                .collect(Collectors.partitioningBy((car) -> { return car.enginePower > 100; }, Collectors.toSet()));
        System.out.println(partitionedSet);

        LongSummaryStatistics statistics = Stream.of(
                new Car("Volvo", 165),
                new Car("Volvo", 165),
                new Car("Honda", 104),
                new Car("Honda", 201)
        ).collect(Collectors.summarizingLong((e) -> {
            return e.enginePower;
        }));
        System.out.println(statistics);



    }

    private static class Car {
        String brand;
        long enginePower;
        Car(String brand, long enginePower) {
            this.brand = brand;
            this.enginePower = enginePower;
        }
        public String getBrand() {
            return brand;
        }
        @Override
        public String toString() {
            return brand + ": " + enginePower;
        }
    }

    static void fileReader () {
        String filePath = //System.getProperty("user.dir") + File.separator +
                "src/main/resources/authors.csv";
        try (Stream<String> authors = Files.lines(Paths.get(filePath))) {
            authors.forEach((author) -> {
                System.out.println(author);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println();

        try (Stream<String> authors = new BufferedReader(
                new InputStreamReader(new FileInputStream(filePath))).lines()
        ) {
            authors.forEach((author) -> {
                System.out.println(author);
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
