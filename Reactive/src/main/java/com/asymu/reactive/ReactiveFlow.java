package com.asymu.reactive;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.Stream;

public class ReactiveFlow {
    public static void main(String[] args) {
        SubmissionPublisher<String> submissionPublisher =  new SubmissionPublisher<>();
        LipsumSubscriber lipsumSubscriber = new LipsumSubscriber();
        //subscribe to a publisher
        submissionPublisher.subscribe(lipsumSubscriber);

        String fileName = "src/main/resources/lipsum.txt";

        try (Stream<String> lines = Files.lines(Path.of(fileName))) {
            //submit the data via publisher
            lines.flatMap((l) -> Arrays.stream(l.split("[\\s.,\\n]+"))).forEach(submissionPublisher::submit);
            //notify subscriber the stream has ended
            submissionPublisher.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
