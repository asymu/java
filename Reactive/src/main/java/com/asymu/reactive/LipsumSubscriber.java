package com.asymu.reactive;

import java.util.concurrent.Flow;

public class LipsumSubscriber implements Flow.Subscriber<String>{

    private Flow.Subscription subscription;
//    onSubscribe method will be called by the publisher when the Subscription object has been created
    @Override
    public void onSubscribe(Flow.Subscription subscription) {
    this.subscription = subscription;
    this.subscription.request(1);
    }

    @Override
    public void onNext(String item) {
        System.out.println(item);
        this.subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable.getMessage());
    }

    @Override
    public void onComplete() {
        System.out.println("Completed!!");
    }
}
