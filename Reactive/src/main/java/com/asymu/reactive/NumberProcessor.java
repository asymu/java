package com.asymu.reactive;

import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.function.Predicate;

public class NumberProcessor extends SubmissionPublisher<String> implements Flow.Processor<String, String> {

    /*NumberProcessor will subscribe to NumberPublisher, and, just like the subscriber,
    it needs to store a reference to the publisher so that it can control when to request new items.
    Store the reference received in onSubscribe() as a private field in the processor.
    Also, take this opportunity to request the first item from the publisher*/

    private Flow.Subscription subscription;


    //since we know that there may be invalid values, we want to filter those
    private Predicate<String> predicate = new Predicate<String>() {
        @Override
        public boolean test(String s) {
            try {
                Integer.valueOf(s);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    };

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        this.subscription.request(1);
    }

    @Override
    public void onNext(String item) {
        if (predicate.test(item)) {
            submit(item);
        }
        this.subscription.request(1);
    }

    /*If the subscription to NumberPublisher is closed, we also need to inform the subscriber
    that there was a problem. Likewise, we need to inform the subscriber when the subscription ended.
    In the onError() callback, add a call to closeExceptionally(),
    and, in onComplete(), add a call to close()*/

    @Override
    public void onError(Throwable throwable) {
        closeExceptionally(throwable);
    }

    @Override
    public void onComplete() {
        close();
    }
}
