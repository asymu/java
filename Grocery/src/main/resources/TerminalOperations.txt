Terminal Operations
Every pipeline needs to end with a terminal operation; without this, the pipeline will not be executed. Unlike intermediate operations, terminal operations may have various return values as they mark the end of the pipeline. You cannot apply another operation after a terminal operation.

Note: When a terminal operation is applied to a stream, you cannot use that stream again. Therefore, storing references to streams in code can cause confusion as to how that reference might be used – you're not allowed to "split" a stream into two different use cases. If you attempt to apply operations on a stream that already had the terminal operation executed, then it will throw an IllegalStateException with the message stream has already been operated upon or closed.

There are 16 different terminal operations in the Stream API – each of them with their own specific use cases. The following is an explanation of each of them:

forEach: This terminal operator acts like a normal for loop – it will run some code for each element in the stream. This is not a thread-safe operation, so you'll need to provide synchronization should you find yourself using shared state:
Stream.of(1, 4, 6, 2, 3, 7).forEach((n) -> { System.out.println(n); });
If this operation is applied on a parallel pipeline, the order in which elements are acted on will not be guaranteed:

Stream.of(1, 4, 6, 2, 3, 7).parallel().forEach((n) -> { System.out.println(n); });
If the order in which the elements are acted on matters, you should use the forEachOrdered() terminal operation instead.

forEachOrdered: Much like the forEach() terminal operation, this will allow you to perform an action for each element in the stream. However, the forEachOrdered() operation will guarantee the order in which elements are processed, regardless of how many threads they're processed on:
Stream.of(1, 4, 6, 2, 3, 7).parallel().forEachOrdered((n) -> { System.out.println(n); });
Here, you can see a parallel stream with a defined encounter order. Using the forEachOrdered() operation, it will always encounter elements in the natural, indexed order.

ToArray: These two terminal operations will allow you to convert the elements of the stream into an array. The basic version will generate an Object array:
Object[] array = Stream.of(1, 4, 6, 2, 3, 7).toArray();
If you need a specific type of array, you can supply a constructor reference for the type of array you need:

Integer[] array = Stream.of(1, 4, 6, 2, 3, 7).toArray(Integer[]::new);
A third option is to also write your own generator for the toArray() operation:

Integer[] array = Stream.of(1, 4, 6, 2, 3, 7).toArray(elements -> new Integer[elements]);
Reduce: To perform a reduction on a stream means to only extract the interesting parts of the elements of that stream and reduce them to a single value. There are two generic reduce operations available. The first, simpler one, takes an accumulator function as an argument. It is usually used after a map operation is applied on a stream:
int sum = Stream.of(1, 7, 4, 3, 9, 6).reduce(0, (a, b) -> a + b);
The second, more complex version takes an identity that also acts as the initial value of the reduction. It also requires an accumulator function where the reduction takes place, as well as a combining function to define how two elements are reduced:

int sum = Stream.of(1, 7, 4, 3, 9, 6).reduce(0, (total, i) -> total + i, (a, b) -> a + b );
In this example, the accumulator adds up the result of the combining function to the identity value, which, in this case, is the total sum of the reduction.

Sum: This is a more specific reduction operation, which will sum all elements in the stream. This terminal operation is only available for IntStream, LongStream, and DoubleStream. To use this functionality in a more generic stream, you would have to implement a pipeline using the reduce() operation, usually preceded by a map() operation. The following example illustrates the use of IntStream:
int intSum = IntStream.of(1, 7, 4, 3, 9, 6).sum();
System.out.println(intSum);
This will print the result as 30. The following example illustrates the use of LongStream:

long longSum = LongStream.of(7L, 4L, 9L, 2L).sum();
System.out.println(longSum);
This will print the result as 22. The following example illustrates the use of DoubleStream:

double doubleSum = DoubleStream.of(5.4, 1.9, 7.2, 6.1).sum();
System.out.println(doubleSum);
This will print the result as 20.6.

Collect: The collection operation is like the reduce operation, in that it takes the elements of a stream and creates a new result. However, instead of reducing the stream to a single value, collect can take the elements and generate a new container or collection that holds all the remaining elements; for example, a list. Usually, you would use the Collectors help class, as it contains a lot of ready-to-use collect operations:
List<Integer> items = Stream.of(6, 3, 8, 12, 3, 9).collect(Collectors.toList());
System.out.println(items);
This would print [6, 3, 8, 12, 3, 9] to the console. You can review more usages of Collectors in the Using Collectors section. Another option is to write your own supplier, accumulator, and combiner for the collect() operation:

List<Integer> items = Stream.of(6, 3, 8, 12, 3, 9).collect(
         () -> { return new ArrayList<Integer>(); },
         (list, i) -> { list.add(i); },
         (list, elements) -> { list.addAll(elements); });
System.out.println(items);
This can, of course, be simplified in this example by using method references:

List<Integer> items = Stream.of(6, 3, 8, 12, 3, 9).collect(ArrayList::new, List::add, List::addAll);
System.out.println(items);
Min: As the name suggests, this terminal operation will return the minimum value, wrapped in an Optional, of all elements in the stream specified according to a Comparator. In most cases, you'd use the Comparator.comparingInt(), Comparator.comparingLong(), or Comparator.comparingDouble() static helper functions when applying this operation:
Optional min = Stream.of(6, 3, 8, 12, 3, 9).min((a, b) -> { return a - b;});
System.out.println(min);
This should write Optional[3].

Max: The opposite of the min() operation, the max() operation returns the value of the element with the maximum value according to a specified Comparator, wrapped in an Optional:
Optional max = Stream.of(6, 3, 8, 12, 3, 9).max((a, b) -> { return a - b;});
System.out.println(max);
This will print Optional[12] to the terminal.

Average: This is a special type of terminal operation that is only available on IntStream, LongStream, and DoubleStream. It returns an OptionalDouble containing the average of all elements in the stream:
OptionalDouble avg = IntStream.of(6, 3, 8, 12, 3, 9).average();
System.out.println(avg);
This will give you an Optional with the containing value 6.833333333333333.

Count: This is a simple terminal operator returning the number of elements in the stream. It's worth noting that, sometimes, the count() terminal operation will find more efficient ways of calculating the size of the stream – in these cases, the pipeline will not even be executed:
long count = Stream.of(6, 3, 8, 12, 3, 9).count();
System.out.println(count);
anyMatch: The anyMatch() terminal operator will return true if any of the elements in the stream match the specified predicate:
boolean matched = Stream.of(6, 3, 8, 12, 3, 9).anyMatch((e) -> { return e > 10; });
System.out.println(matched);
As there is an element with a value above 10, this pipeline will return true.

allMatch: The allMatch() terminal operator will return true if all the elements in the stream match the specified predicate:
boolean matched = Stream.of(6, 3, 8, 12, 3, 9).allMatch((e) -> { return e > 10; });
System.out.println(matched);
Since this source has elements whose values are below 10, it should return false.

noneMatch: Opposite to allMatch(), the noneMatch() terminal operator will return true if none of the elements in the stream match the specified predicate:
boolean matched = Stream.of(6, 3, 8, 12, 3, 9).noneMatch((e) -> { return e > 10; });
System.out.println(matched);
Because the stream has elements of values above 10, this will also return false.

findFirst: This retrieves the first element of the stream, wrapped in an Optional:
Optional firstElement = Stream.of(6, 3, 8, 12, 3, 9).findFirst();
System.out.println(firstElement);
This will print Optional[6] to the terminal. If there were no elements in the stream, it would instead print Optional.empty.

findAny: Much like the findFirst() terminal operation, the findAny() operation will return an element wrapped in an Optional. This operation, however, will return any one of the elements that remain. You should never really assume which element it will return. This operation will, usually, act faster than the findFirst() operation, especially in parallel streams. It's ideal when you just need to know whether there are any elements left but don't really care about which remain:
Optional firstElement = Stream.of(7, 9, 3, 4, 1).findAny();
System.out.println(firstElement);
Iterator: This is a terminal operator that generates an iterator that lets you traverse elements:
Iterator<Integer> iterator = Stream.of(1, 2, 3, 4, 5, 6).iterator();
while (iterator.hasNext()) {
    Integer next = iterator.next();
    System.out.println(next);
}
SummaryStatistics>: This is a special terminal operation that is available for IntStream, LongStream, and DoubleStream. It will return a special type – for example, IntSummaryStatistics – describing the elements of the stream:
IntSummaryStatistics intStats = IntStream.of(7, 9, 3, 4, 1).summaryStatistics();
System.out.println(intStats);
LongSummaryStatistics longStats = LongStream.of(6L, 4L, 1L, 3L, 7L).summaryStatistics();
System.out.println(longStats);
DoubleSummaryStatistics doubleStats = DoubleStream.of(4.3, 5.1, 9.4, 1.3, 3.9).summaryStatistics();
System.out.println(doubleStats);
This will print all the summaries of the three streams to the terminal, which should look like this:

IntSummaryStatistics{count=5, sum=24, min=1, average=4,800000, max=9}
LongSummaryStatistics{count=5, sum=21, min=1, average=4,200000, max=7}
DoubleSummaryStatistics{count=5, sum=24,000000, min=1,300000, average=4,800000, max=9,400000}