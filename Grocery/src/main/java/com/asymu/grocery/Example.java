package com.asymu.grocery;

import java.util.stream.Stream;

public class Example {

    public static void main(String[] args) {
//        Stream.of(1,2,3).parallel().sequential().forEach(i -> System.out.println(Thread.currentThread() +"\ti="+i));

        Stream.of(1,2,3)
                .parallel()
                .map(i -> {
                    System.out.println(Thread.currentThread() +"\ti="+i);
                    return i;
                })
                .sequential()
                .forEach(i -> System.out.println(Thread.currentThread() +"\ti="+i));

    }
}
