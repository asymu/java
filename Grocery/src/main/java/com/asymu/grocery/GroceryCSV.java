package com.asymu.grocery;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GroceryCSV {

    static String filename = "src/main/resources/database.csv";
    public static void main(String[] args) {

        List<ShoppingArticle> database = csvToList();

        System.out.println(database);

        System.out.println(database);

        System.out.println("Cheapest fruit: " + findCheapestFruit(database));

        System.out.println("Most expensive vegetable: " + findMostExpensiveVegetable(database));

        System.out.println("Fruits: " + findFruits(database));

        System.out.println("Five most expensive articles: " + findFiveMostExpensive(database));

        System.out.println("Five cheapest articles: " + findFiveCheapest(database));
    }


    static List<ShoppingArticle> csvToList() {


        try (Stream<String> stream = Files.lines(Path.of(filename))) {

            return stream
                   // .peek((line) -> {System.out.println(line);})
                    .skip(1) //heAder
                    .map((line) -> {return line.split(",");}) // Stream<String[]>
                    //  .peek(arr -> System.out.println(Arrays.toString(arr)))
                    .map((line) -> new ShoppingArticle(line[0], line[1], Double.valueOf(line[2]), line[3])) //Stream<ShoppingArticle>
                    //  .peek(arr -> System.out.println(arr))
//            Use an unmodifiable list to protect from unwanted modifications
                    .collect(Collectors.toUnmodifiableList());

        } catch ( IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static ShoppingArticle findCheapestFruit (List<ShoppingArticle> articles) {
        return articles.stream()
                .filter((article) -> article.category.equals("Fruits"))
                .min(Comparator.comparingDouble(article -> article.price))
                .orElse(null);
    }

    private static ShoppingArticle findMostExpensiveVegetable (List<ShoppingArticle> articles) {
        return articles.stream()
                .filter((article) -> article.category.equals("Vegetables"))
                .max(Comparator.comparingDouble(article -> article.price))
                .orElse(null);
    }

    private static List<ShoppingArticle> findFruits (List<ShoppingArticle> articles) {
        return articles.stream()
                .filter((article) -> article.category.equals("Fruits"))
                .collect(Collectors.toList());
    }

    private static List<ShoppingArticle> findFiveCheapest (List<ShoppingArticle> articles) {
        return articles.stream()
                .sorted(Comparator.comparingDouble(article -> article.price))
                .limit(5)
                .collect(Collectors.toList());
    }

    private static List<ShoppingArticle> findFiveMostExpensive (List<ShoppingArticle> articles) {
        return articles.stream()
                .sorted((article1, article2) -> Double.compare(article2.price, article1.price))
                .limit(5)
                .collect(Collectors.toList());
    }

    private static class ShoppingArticle {
        final String name;
        final String category;
        final double price;
        final String unit;
        private ShoppingArticle(String name, String category, double price, String unit) {
            this.name = name;
            this.category = category;
            this.price = price;
            this.unit = unit;
        }

        @Override
        public String toString() {
            return "ShoppingArticle{" +
                    "name='" + name + '\'' +
                    ", category='" + category + '\'' +
                    ", price=" + price +
                    ", unit='" + unit + '\'' +
                    '}';
        }
    }
}
