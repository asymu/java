package com.asymu.steps;

import java.time.LocalDate;

/**
 * Created by Syed on 07-Apr-20.
 */
public class Steps {

    private int steps;
    private LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public Steps(int steps, LocalDate date) {
        this.steps = steps;

        this.date = date;
    }
}