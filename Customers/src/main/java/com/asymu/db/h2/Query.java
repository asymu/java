package com.asymu.db.h2;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Query {

    static Logger logger = Logger.getAnonymousLogger();

    public static void main(String[] args) {
//        selectQuery();
//        updateUsingPreparedStmt();
        if (args.length == 3) {
            updateChapterComplete(args);
        } else {
            progress(args);
        }
    }

    private static void updateChapterComplete(String[] args) {
        if (args.length == 3) {
            updateChapter(args[0], args[1], args[2]);
        }

    }

    static void updateChapter(String fname, String lname, String chapterName) {
        {
            Connection conn = null;
            PreparedStatement statement = null;
            String sql = "insert into chapter ( chapter_name, completion_dt, student_id) " +
                    " values( ? ,  CURRENT_DATE, (select id from student where fname = ? and lname = ?)) ";

            try {
                conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
                conn.setAutoCommit(false); //program to manage transactions if set to false
                statement = conn.prepareStatement(sql);
//                statement.setInt(1, idcounter);
                statement.setString(1, chapterName);
                statement.setString(2, fname);
                statement.setString(3, lname);
                statement.executeUpdate();// executeUpdate for updating
                conn.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                try {
                    if (conn != null) {
                        conn.rollback();
                    }
                } catch (SQLException nested) {
                    nested.printStackTrace();
                }
            } finally {
                try {
                    conn.setAutoCommit(true);
                    if (statement != null) {
                        statement.close();
                    }
                    conn.close();
                } catch (SQLException nested) {
                    nested.printStackTrace();
                }
            }
        }
    }

    private static void progress(String[] args) {
        if (args.length ==2) {
            completedChapter(args[0], args[1]);
        }
    }

    private static void completedChapter(String fname, String lname) {
        {
            String sql = "SELECT * from chapter where student_id =  (select id from student where fname = '" +fname+"' and lname ='"+lname+"')";
            Statement statement;
            Connection conn;
            try {
                conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
                statement = conn.createStatement();
                ResultSet results = statement.executeQuery(sql);
                while (results.next()) {
                    String chapterName = results.getString("chapter_name");
                    String completionDt = results.getDate("completion_dt").toString();
                    System.out.println(chapterName +" completed on " + completionDt);
                }
                if (results != null) {
                    results.close();
                }
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                logger.log(Level.SEVERE, e.getMessage());
            } finally {
                // use if required to close SQL objs'
            }
        }

    }

    static void selectQuery(){
        {
            String sql = "SELECT * from customer order by username";
            Statement statement;
            Connection conn;
            try {
                conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
                statement = conn.createStatement();
                ResultSet results = statement.executeQuery(sql);
                while (results.next()) {
                    Long id = results.getLong("CUSTOMER_ID");
                    String username = results.getString("USERNAME");
                    String firstName = results.getString("FIRST_NAME");
                    String lastName = results.getString("LAST_NAME");
                    System.out.println(id + " " + username + " " +
                            firstName + " " + lastName);
                }
                if (results != null) {
                    results.close();
                }
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                logger.log(Level.SEVERE, e.getMessage());
            } finally {
                // use if required to close SQL objs'
            }
        }
    }

    static void updateUsingPreparedStmt() {
        Connection conn = null;
        PreparedStatement statement = null;
        String sql = "UPDATE email " +
                "SET EMAIL_ADDRESS = ? " +
                "WHERE customer_id = ? " +
                "AND email_type = ? ";

        try {
            conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
            conn.setAutoCommit(false); //program to manage transactions if set to false
            statement = conn.prepareStatement(sql);
            statement.setString(1, "syed.m@company.com");
            statement.setLong(2, 1L);
            statement.setString(3, "WORK");
            int rowsChanged = statement.executeUpdate();// executeUpdate for updating
            conn.commit();
            System.out.println("Number rows changed: " + rowsChanged);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException nested) {
                nested.printStackTrace();
            }
        } finally {
            try {
                conn.setAutoCommit(true);
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException nested) {
                nested.printStackTrace();
            }
        }
    }
}


//    create table STUDENT(
//        Id INT,
//        Fname varchar(255),
//    Lname varchar(255),
//    UNIQUE(Fname, Lname),
//    Primary key(Id)
//);

//    create table chapter(
//        Id INT,
//        Chapter_name varchar(255),
//    Student_id varchar(255),
//    Primary key(Id)
//);

//    insert into student(id, fname, lname) values (1, 'syed', 'mudassir')

//    alter table chapter add column (completion_dt datetime)

//    alter table chapter ALTER column completion_dt  date

//    alter table chapter alter column id int AUTO_INCREMENT

//java -jar Customers-1.0-all.jar syed mudassir chp4 // to update completed chapter

//java -jar Customers-1.0-all.jar syed mudassir // to see progress

