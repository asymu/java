package com.asymu.day;

import java.time.LocalDate;

/**
 * Created by Syed on 07-Apr-20.
 */
public class Today {

    public String getToday() {
        return LocalDate.now().getDayOfWeek().toString();
    }
}
