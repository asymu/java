package com.asymu.pattern;

public class Fibonacci {

    public static void main(String[] args) {
//        System.out.println(fact(5));
        System.out.println(fib(2));
    }

    static int fact (int number) {
        if (number == 1) {
            return 1;
        } else {
            return number * fact(number - 1);
        }
    }
    static int first = 0;
    static int second = 1;
    static int returnFib = 1;

    static int  fib (int num) {
        if (num == 1) {
            return returnFib;
        } else {
            returnFib = first + second;
            first = second;
            second = returnFib;
            fib(num - 1);
        }
        return returnFib;
    }
}
