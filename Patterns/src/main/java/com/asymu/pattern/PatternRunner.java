package com.asymu.pattern;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternRunner {

    public static void main(String[] args) {
//        findCorrectAddress();
//        extractUrl();
        matchDemoFile();
    }

    static void findCorrectAddress() {
        Pattern pattern = Pattern.compile("[a-zA-Z]{2,}\\s{1}\\d{1}\\w");
        // [a-zA-Z]{2,}\s{1}\d+[a-zA-Z]+
        Matcher matcher = pattern.matcher("Strandvagen 1s");
        Boolean matches = matcher.matches();
        System.out.printf("Match found: " + matches);
    }

    static void extractUrl() {
        String url = "https://www.packtpub.com/application-development/mastering-java-9";
        String regex = "(http[s])(?:://)([w]{0,3}\\.?[a-zA-Z]+\\.[a-zA-Z]{2,3})(?:[/])(.*)";
        System.out.println(regex);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(url);
        boolean foundMatches = matcher.find();
        if (foundMatches) {
            String protocol = matcher.group(1);
            String domain = matcher.group(2);
            String path = matcher.group(3);
            System.out.println("Protocol: " + protocol);
            System.out.println("domain: " + domain);
            System.out.println("Path: " + path);
        }
    }

    static  void matchDemoFile () {
        String filePath = /*System.getProperty("user.dir") +*/ /*File.separator +*/
                "src/main/resources" + File.separator + "demo.txt";
        try {
            String packtDump = new String(Files.readAllBytes(Paths.get(filePath)));
            String regex = "(?:<a href=\")([^\"]+)(?:\"{1})";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(packtDump);
            List<String> links = new ArrayList<>();
            while (matcher.find()) {
                links.add(matcher.group(1));
            }
            System.out.println(links);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
