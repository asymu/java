package com.asymu.process;

import java.io.*;
import java.util.concurrent.TimeUnit;

public class Processes {
    public static void main(String[] args) throws IOException {
//        lookProcesses();
//        launchProcess();
//        launchProcessUsingProcessBuilder();
//        childProcess();
        test();
    }
    static void lookProcesses(){
        Runtime runtime = Runtime.getRuntime();
        System.out.println("Available Processors: " + runtime.availableProcessors());
        System.out.println("Total memory: " + runtime.totalMemory());
        System.out.println("Free memory: " + runtime.freeMemory());
    }

    static void launchProcess(){
        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        try {
            process = runtime.exec("notepad");
        } catch (IOException ioe) {
            System.out.println("WARNING: something happened with exec");
        }
        try {
            process.waitFor(5, TimeUnit.SECONDS);
        } catch (InterruptedException ie) {
            System.out.println("WARNING: interruption happened");
        }
        process.destroy();
    }

    static void launchProcessUsingProcessBuilder() {
        ProcessBuilder processBuilder = new ProcessBuilder("notepad");
        Process process = null;
        try {
            process = processBuilder.start();
        } catch (IOException ioe) {
            System.out.println("WARNING: something happened with exec");
        }
        try {
            process.waitFor(05, TimeUnit.SECONDS);
        } catch (InterruptedException ie) {
            System.out.println("WARNING: interruption happened");
        }
        process.destroy();
    }

    static void childProcess() throws IOException {
        int c;
        System.out.print ("Let's echo: ");
        while ((c = System.in.read ()) != '\n')
            System.out.print ((char) c);
    }

    static void invokeOtherJava() throws IOException {
        // process = runtime.exec(
        //   "java -cp ../../JavaProgramPath; Params");

        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        try {
            // for Windows
             process = runtime.exec("java -cp ../../JavaProgramPath; Params");
            //   "java -cp ../../JavaProgramPath; Params");
        } catch (IOException ioe) {
            System.out.println("WARNING: couldn't start your app");
        }
        try {
            process.waitFor(5, TimeUnit.SECONDS);
        } catch (InterruptedException ie) {
            System.out.println("WARNING: interrupted exception fired");
        }
        OutputStream out = process.getOutputStream();
        Writer writer = new OutputStreamWriter(out);
        writer.write("This is how we roll!\n"); // EOL to ensure the process sends back
        writer.flush();
        process.destroy();

        //read your programs output
        InputStream in = process.getInputStream();
        Reader reader = new InputStreamReader(in);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line = bufferedReader.readLine();
        System.out.println(line);

        //storing to a file
        /*File file = new File("data.log");
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        // read from System.out from the child
        InputStream in = process.getInputStream();
        Reader reader = new InputStreamReader(in);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line = bufferedReader.readLine();
        // send to screen
        System.out.println(line);
        // send to file
        bufferedWriter.write(line);
        bufferedWriter.flush();
        process.destroy();*/
    }

    static void test(){
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("notepad");
            Process process = processBuilder.start();
            new BufferedReader(new InputStreamReader(process.getInputStream())).lines().limit(20).forEach(System.out::println);
            process.waitFor(5, TimeUnit.SECONDS);
            process.destroyForcibly();
        } catch (InterruptedException | IOException exeption) {
            exeption.printStackTrace();
        }
    }
}
