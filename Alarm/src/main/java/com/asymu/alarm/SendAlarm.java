package com.asymu.alarm;

import java.util.function.Consumer;

public class SendAlarm implements Consumer<Sensor> {
    @Override
    public void accept(Sensor sensor) {
        if (sensor.triggered()) {
            AlarmNotify.alarmServiceNotified = true;
        }
    }
}
