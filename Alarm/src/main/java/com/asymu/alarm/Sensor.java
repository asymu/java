package com.asymu.alarm;

public abstract interface Sensor {
    int batteryHealth();

    void batteryHealth(int health);

    boolean triggered();

    void triggered(boolean state);
}
