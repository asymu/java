package com.asymu.http;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class HttpCon {

    public static void main(String[] args) throws IOException, InterruptedException {
//        httpHeader();
//        hhtpGet();
//        postJson();
//        useJsoup();
//        NetHttpClient();
        NetGet();
    }

    static void httpHeader() throws IOException {
        String path = "http://example.com";
        URL url = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("HEAD");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = connection.getResponseCode();
        System.out.println("Key: Status code, Value: " + responseCode);

        Map<String, List<String>> headers = connection.getHeaderFields();
        for (String key : headers.keySet()) {
            System.out.println("Key: " + key + ", Value: " + headers.get(key));
        }
    }

    static void hhtpGet() throws IOException {
        String path = "http://example.com";
        URL url = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(6000);
        connection.setReadTimeout(6000);
        connection.setInstanceFollowRedirects(false);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream())
        );
        String line;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        in.close();
    }


    static void postJson () {
        /*
        {
            "fname": "Syed",
            "lname": "Mudassir"
        }
        */
        String content = "{ \"fname\": \"Syed\", \"lname\": \"Mudassir\" }";
        String path = "http://httpbin.org/post"; // test site
        try {
            URL url = new URL(path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            DataOutputStream out = new DataOutputStream( connection.getOutputStream() );
            out.writeBytes(content);
            out.flush();
            out.close();
            int responseCode = connection.getResponseCode();
            System.out.println("Code: " + responseCode);
            if (responseCode != HttpURLConnection.HTTP_OK) {
                System.out.println("Got an unexpected response code");
            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            );
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void useJsoup() {
        try {
            String path = "https://docs.oracle.com/en/java/javase/12/";
            Document doc = Jsoup.connect(path).get();
            Elements topics = doc.select("ul.topics");
            for (Element topic : topics) {
                for (Element listItem : topic.children()) {
                    for (Element link : listItem.children()) {
                        String url = link.attr("href");
                        String text = link.text();
                        System.out.println(url + " " + text);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void NetHttpClient() {
        HttpClient client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(30))
                .build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://example.com/"))
                .timeout(Duration.ofSeconds(30))
                .header("Accept", "text/html")
                .build();
        HttpResponse<String> response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Code: " + response.statusCode());
        System.out.println(response.body());
    }

    static void NetGet() throws IOException, InterruptedException {
       /* HttpClient client = HttpClient.newBuilder().
                version(HttpClient.Version.HTTP_2).
                followRedirects(HttpClient.Redirect.NORMAL).
                connectTimeout(Duration.ofSeconds(40)).build();

        HttpRequest request = HttpRequest.newBuilder().
                uri(URI.create("https://www.packtpub.com/tech/java")).
                timeout(Duration.ofSeconds(30)).header("Accept", "text/html").build();

        HttpResponse<String> response = null;
        response = client.send(request, HttpResponse.BodyHandlers.ofString());*/

        // css class book-block-title

        Document doc = Jsoup.connect("http://www.packtpub.com/tech/java/").get();
        Elements topics = doc.select("book-block-title");
        for (Element topic : topics) {
            for (Element listItem : topic.children()) {
                for (Element link : listItem.children()) {
                    String url = link.attr("div");
                    String text = link.text();
                    System.out.println(url + " " + text);
                }
            }
        }


    }
}

