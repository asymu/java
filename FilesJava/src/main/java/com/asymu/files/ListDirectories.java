package com.asymu.files;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListDirectories {

    static Logger logger = Logger.getAnonymousLogger();

    public static void main(String[] args) throws IOException {
//    listDirsIO();
//    listDirNIO();
//    listDirUsingStreamNIO();
//        listSubDirNIO();
//        additional();
//        createNwriteToDirFile();
//    dirStr();
//        reaFile();
//        readProperties();
        test();
    }

    static void listDirsIO() {
        // using java.io
        String filePath = System.getProperty("java.io.tmpdir");
        String fileNames[] = new File(filePath).list();
        for (int i = 0; i < 5; i++) {
            System.out.println(fileNames[i]);
        }
    }

    static void listDirNIO() {
        String filePath = System.getProperty("java.io.tmpdir");
        List<String> fileNames = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(filePath))) {
            for (Path path : directoryStream) {
                fileNames.add(path.toString());
            }

           /* for (int i = 0; i < 5; i++){
                System.out.println(fileNames.get(i));
            }*/

            // tell if this is file or dir
            for (int i = 0; i < 5; i++) {
                String filePath1 = fileNames.get(i);
                String fileType = Files.isDirectory(Paths.get(filePath1)) ? "Dir : " : "File: ";
                System.out.println(fileType + " " + filePath1);
            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    static void listDirUsingStreamNIO() throws IOException {
        String filePath = System.getProperty("java.io.temp");

        if (filePath == null) {
            filePath = "E:\\Development";
        }

        if (filePath != null) {
            Path path = Paths.get(filePath);
            Stream<Path> fileNames = Files.list(path);
//        fileNames.limit(5).forEach(System.out::println);

//        Stream<Path> fileNames = Files.list(path).filter(Files::isDirectory);
//        fileNames.limit(5).forEach(System.out::println);

            // to list sub dir
            fileNames.limit(5).forEach((item) -> {
                System.out.println(item.toString());
                try {
                    Stream<Path> fileNames2 = Files.list(item).filter(Files::isDirectory);
                    fileNames2.forEach(System.out::println);
                } catch (IOException ioe) {
                }
            });
        } else {
            logger.log(Level.SEVERE, "file name is null!!");
        }
    }

    static void listSubDirNIO() throws IOException {
        String filePath = System.getProperty("user.home");
        List<Path> subDir = Files.walk(Paths.get(filePath), 1).
                filter(Files::isDirectory).
                collect(Collectors.toList());
        for (int i = 0; i < 5; i++) {
            Path filpath = subDir.get(i);
            String dirType = Files.isDirectory(filpath) ? "Dir" : "Fil";
            System.out.println(dirType + " " + filpath);
        }
    }

    static void additional() throws IOException {
        Path path = Paths.get(System.getProperty("user.home"));

        Files.walkFileTree(path, Collections.emptySet(), 2, new SimpleFileVisitor() {
            //            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                System.out.println(dir.toString());
                return FileVisitResult.CONTINUE;
            }

            //            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                System.out.println("visitFileFailed: " + file);
                return FileVisitResult.CONTINUE;
            }
        });

    }

    static void createNwriteToDirFile() {
        String pathString = System.getProperty("user.home") + "/javaTemp/";

        if (pathString == null) {
            pathString = "E:\\Development";
        }

        Path pathDirectory = Paths.get(pathString);
        if (Files.exists(pathDirectory)) {
            System.out.println("WARNING: directory exists already at: " + pathString);
        } else {
            try {
                // Create the directory
                Files.createDirectories(pathDirectory);
                System.out.println("New directory created at: " + pathString);
            } catch (IOException ioe) {
                System.out.println("Could not create the directory");
                System.out.println("EXCEPTION: " + ioe.getMessage());
            }
        }
        String fileName = "temp.txt";
        Path pathFile = Paths.get(pathString + fileName);
        if (Files.exists(pathFile)) {
            System.out.println("WARNING: file exists already at: " + pathFile);
        } else {
            try {
                // Create the file
                Files.createFile(pathFile);
                System.out.println("New file created at: " + pathFile);
            } catch (IOException ioe) {
                System.out.println("Could not create the file");
                System.out.println("EXCEPTION: " + ioe.getMessage());
            }
        }

        String text = "Hello,\nWelcome to Java.";
        if (Files.exists(pathFile))
            try {
                Files.write(pathFile, Arrays.asList(text), StandardOpenOption.APPEND);
                System.out.println("Text added to the file: " + pathFile);
            } catch (IOException ioe) {
                System.out.println("EXCEPTION: " + ioe.getMessage());

            }
    }

    static void dirStr() throws IOException {
        Path path = Paths.get("E:\\Development\\tutorials-master");
        System.out.println(path);

        String fileName = "tem.txt";
        Path pathFile = Paths.get(fileName);

        if(!Files.exists(pathFile))  {
            try {
                // Create the file
                Files.createFile(pathFile);
                System.out.println("New file created at: " + pathFile.toString());
            } catch (IOException ioe) {
                System.out.println("EXCEPTION when creating file: " + ioe.getMessage());
            }
        }

        Files.walkFileTree(path, Collections.emptySet(), 10, new
                SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {

                        String toFile = "";

                        String [] pathArray = path.toString().split("/");
                        int depthInit = pathArray.length;
                        String [] fileArray = dir.toString().split("/");
                        int depthCurrent = fileArray.length;
                        for (int i = depthInit; i < depthCurrent; i++) {
                            toFile += "    ";
                        }
                        toFile += fileArray[fileArray.length - 1];

                        if(Files.exists(pathFile))
                            try {
                                Files.write(pathFile, Arrays.asList(toFile),
                                        StandardOpenOption.APPEND);
                            } catch (IOException ioe) {
                                System.out.println("EXCEPTION when writing to file: " + ioe.getMessage());
                            }

                        return FileVisitResult.CONTINUE;
                    }
                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException
                            exc)
                            throws IOException {
//                        System.out.println("visitFileFailed: " + file);
                        return FileVisitResult.CONTINUE;
                    }
                });
    }

    static void reaFile() {
        Path path = Paths.get("C:\\Users\\Syed\\.pgAdmin4.427924103.log");

        try {
           /* List<String> allLines = Files.readAllLines(path);

//            for (String s : allLines) {
//                System.out.println(s);
//            }

            allLines.forEach(System.out::println);*/

            //approach 2
            Files.lines(path).forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void readProperties() throws IOException {
        String pathString = System.getProperty("user.home") + "/javaTemp/user.properties";
        FileInputStream fileStream = null;
        try {
            fileStream = new FileInputStream(pathString);
            Properties properties = new Properties();
            properties.load(fileStream);
            System.out.println(properties.getProperty("fname"));
            System.out.println(properties.getProperty("lname"));
        } catch (FileNotFoundException fnfe) {
            System.out.println("WARNING: could not find the properties file");
        } catch (IOException ioe) {
            System.out.println("WARNING: problem processing the properties file");
        } finally {
            if (fileStream != null) {
                fileStream.close();
            }
        }

    }

    static void WriteProperties(String pathString, Properties
            properties) throws IOException {
        // create the output Stream and write to the file
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(pathString);

            // write the resulting properties object back to the file
            // adds as comment the timestamp to the latest modification made
            properties.store(fileOutputStream, "# modified on: " + java.time.LocalDate.now());
        } catch (FileNotFoundException fnfe) {
            System.out.println("WARNING: could not find the properties file");
        } catch (IOException ioe) {
            System.out.println("WARNING: problem processing the properties file");
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
    }

    static  void test () throws IOException {
//        Arrays.stream(new File("E:\\Development\\tutorials-master").listFiles()).forEach(System.out::println);
//        Files.newDirectoryStream(Paths.get("E:\\\\Development\\\\tutorials-master")).forEach(System.out::println);
//        Files.list(Paths.get("E:\\Development\\tutorials-master")).forEach(System.out::println);
        Paths.get("E:\\Development\\tutorials-master").getFileSystem().getRootDirectories().forEach(System.out::println);
    }
}
