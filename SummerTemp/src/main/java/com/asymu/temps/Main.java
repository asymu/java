package com.asymu.temps;

public class Main {

    public static void main(String[] args) {
        SummerHighs summerHighs = new SummerHighs();
        summerHighs.initialize();
        boolean fahrenheit = false;
// Handle inputs
        if (args.length < 2) {
            System.err.println("Error: usage is:");
            System.err.println(" -city London");
            System.err.println(" -country United Kingdom");
        }
        String searchBy = args[0];
        String name = args[1];
        SummerHigh high = null;
        if ("-city".equals(searchBy)) {
            high = summerHighs.getByCity(name);
        } else if ("-country".equals(searchBy)) {
            high = summerHighs.getByCountry(name);
        }
    }
}
