package com.asymu.steps;

import java.time.LocalDate;

/**
 * Created by Syed on 07-Apr-20.
 */
public class Main {

    public static void main(String[] args) {
        // Initialize sample data.
        DailyGoal dailyGoal = new DailyGoal(10000);
        WeeklySteps weekly = new WeeklySteps();
        weekly.setDailyGoal(dailyGoal);
        int year = 2021;
        int month = 1;
        int day = 4;
        weekly.addDailySteps(11543, LocalDate.of(year, month, day));
        day++;
        weekly.addDailySteps(12112, LocalDate.of(year, month, day));
        day++;
        weekly.addDailySteps(10005, LocalDate.of(year, month, day));
        day++;
        weekly.addDailySteps(10011, LocalDate.of(year, month, day));
        day++;
        weekly.addDailySteps(9000, LocalDate.of(year, month, day));
        day++;
        weekly.addDailySteps(20053, LocalDate.of(year, month, day));
        day++;
        weekly.addDailySteps(20048, LocalDate.of(year, month, day));
        System.out.println(weekly.format());
    }

    /*steps to create a jar from package
    * cd src
    * javac com/asymu/steps/*.java
    * jar cvf fitness.jar  com/asymu/steps/*.class*/

    /*To extract all the files from a JAR file, use the jar xvf command:

    jar xvf fitness.jar

    and

    to see what is inside jar file
    jar tf fitness.jar*/

    /*steps to create a jar from package with MANIFEST file defined the main class
    * cd src
    * javac com/asymu/steps*//*.java
    * jar cvfe fitness.jar com.asymu.steps.Main com/asymu/steps/*.class*/

}
