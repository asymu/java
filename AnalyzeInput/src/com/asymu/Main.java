package com.asymu;

import java.io.Console;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        //iterator();
//        map();
//        scanner();
//        listSort();
//        scanner();
//        properties();
        listSort();
    }

    static  void properties(){
        Properties properties = new Properties();
        Set setOfKeys;
        String key;
        properties.put("OS", "Ubuntu Linux");
        properties.put("version", "18.04");
        properties.put("language", "English (UK)");
        // iterate through the map
        setOfKeys = properties.keySet();
        Iterator iterator = setOfKeys.iterator();
        while(iterator.hasNext())
        {
            key = (String)iterator.next();
            System.out.println(key + " = " + properties.getProperty(key));
        }
        // looking for URL that not in list
        String value = properties.getProperty("keyboard layout", "not found");
        System.out.println("keyboard layout = " + value);

        System.out.println();
        Properties properties1 = new Properties();
        properties1.put("OS", "Ubuntu Linux");
        properties1.put("version", "18.04");
        properties1.put("language", "English (UK)");
        String oldValue = (String) properties1.setProperty("language1", "German");
        System.out.println(oldValue);
        if (oldValue != null) {
            System.out.println("modified the language property");
        }
        properties1.list(System.out);
    }


    static void listSort(){
        List <Double> array = new ArrayList();
        array.add(5.0);
        array.add(2.2);
        array.add(37.5);
        array.add(3.1);
        array.add(1.3);
        System.out.println("Original list: " + array);
        Collections.sort(array);
        System.out.println("Sorted list: " + array);

//        ListIterator listIterator = array.listIterator(array.size());
        Collections.sort(array, Collections.reverseOrder());
        System.out.println("Reverse Sorted list: " + array);

        List <Double> array2 = new ArrayList<>();
        array2.add(2.2);

        Collections.copy(array, array2);
        System.out.println(Collections.indexOfSubList(array, array2));

    }
    static  void scanner(){
        Scanner in = new Scanner(System.in);
        String line = "";

        ArrayList<String> text = new ArrayList<String>();

        while (!line.equals("*")) {
            line = in.nextLine();

            List<String> lineList = new ArrayList<String>(Arrays.asList(line.split(" ")));
            text.addAll(lineList);

        }

        System.out.println("INPUT: " + text);

        Set <String> textSet = new HashSet <String> ();
        textSet.addAll(text);
        Map map = new HashMap<>();
        List<DataPoint>  dataPoints = new ArrayList<>();

        Iterator iterator = textSet.iterator();
        while (iterator.hasNext()) {
            //point to next element
            String s = (String) iterator.next();
            // get the amount of times this word shows up in the text
            int freq = Collections.frequency(text, s);
            // print out the result
            System.out.println(s + " appears " + freq + " times");
//            map.put(s,freq);
            dataPoints.add(new DataPoint(s, freq));
        }

/*
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(map);

        System.out.println(treeMap.entrySet());*/

        // to sort descending by values
        Collections.sort(dataPoints, Collections.reverseOrder(new SortByValue()));

        System.out.println("Results sorted");
        for (int i=0; i<dataPoints.size(); i++)
            System.out.println(dataPoints.get(i).freq + " times for word " + dataPoints.get(i).value);
    }


    static  void  iterator(){
     /*List array = new ArrayList();
     array.add(5);// auto boxing to Integer
     array.add(2);
     array.add(37);
     Iterator iterator = array.iterator();
     while (iterator.hasNext()) {
         //point to next element
         int i = (int) iterator.next();
         // print elements
         System.out.print(i + " ");
         //array.add(i + 5); // ConcurrentModificationException

         iterator.remove();
     }

*/

        List array = new ArrayList();
        array.add(5.9);// auto boxing to Double
        array.add(2.48);
        array.add(37.5);

        ListIterator listIterator = array.listIterator();

        while (listIterator.hasNext()) {
            //point to next element
            double d = (Double) listIterator.next();
            // round up the decimal number
            listIterator.set(Math.round(d));
        }

        System.out.print(array);
    }

    static void console(){
        ArrayList <String> text = new ArrayList<String>();
        Console cons;
        String line = "";
        while (!line.equals("*")
                && (cons = System.console()) != null
                && (line = cons.readLine()) != null) {
            List<String> lineList = new ArrayList<String>(Arrays.asList(line.split(" ")));
            text.addAll(lineList);
        }
        System.out.println("You typed: " + text);
    }

    static  void map(){
        Map map = new HashMap ();
        map.put("name", "Mudassir");
        map.put("family name", "Syed");
        map.put("address", "Blr");
        map.put("mobile", "9999999");

        Iterator <Map.Entry> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            System.out.print("Key = " + entry.getKey());
            System.out.println( ", Value = " + entry.getValue());
        }
    }
}

class DataPoint {
    String value;
    int freq;

    DataPoint (String val, int fre) {
        this.value = val;
        this.freq = fre;
    }
}

class SortByValue implements Comparator<DataPoint> {

    @Override
    public int compare(DataPoint o1, DataPoint o2) {
        return o1.freq - o2.freq;
    }
}