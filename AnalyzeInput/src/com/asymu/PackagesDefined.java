package com.asymu;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

/**
 * Created by Syed on 07-Apr-20.
 */
public class PackagesDefined {

    public  static void main(String[] a) {
        localTimeDate();
    }

    static void localTimeDate(){
        LocalDateTime localDateTime = LocalDateTime.now();
        DayOfWeek day = localDateTime.getDayOfWeek();
        System.out.println("The week day is: " + day);
    }
}
