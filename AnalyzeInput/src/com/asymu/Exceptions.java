package com.asymu;

import java.util.logging.Logger;

/**
 * Created by Syed on 06-Apr-20.
 */
public class Exceptions {

    static Logger logger = Logger.getAnonymousLogger();

    public static void main(String args[]) throws CustomException{
        LoggingExceptions();
    }

    private static void staticMethod() {
        System.out.println("static method, accessible from null reference");
    }
    private void nonStaticMethod() {
        System.out.print("non-static method, inaccessible from null reference");
    }

    static void callStaticWithNullRefrence() {
        Exceptions object = null;
        object.staticMethod();
        //object.nonStaticMethod();
    }

    public static void LoggingExceptions() throws CustomException {

        String s = null;
        try {
            System.out.println(s.length());
        } catch (NullPointerException ne) {
//            logger.log(Level.SEVERE, "Exception happened", ne);
            throw new CustomException("Exception Occured", ne);
        }
    }
}

